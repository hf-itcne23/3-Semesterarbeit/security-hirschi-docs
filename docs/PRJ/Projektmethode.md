# Projektmethode

### Scrum 

Scrum ist ein agiles Framework zur Bewältigung komplexer Projekte, das iterative und inkrementelle Prozesse nutzt. Die Kernkomponenten von Scrum umfassen:

1. **Product Owner**: Verantwortlich für die Definition der Produktanforderungen und die Priorisierung des Product Backlogs.
2. **Scrum Master**: Unterstützt das Team, indem er Hindernisse beseitigt und dafür sorgt, dass Scrum-Prinzipien eingehalten werden.
3. **Team**: Selbstorganisierende Gruppe, die das Produkt erstellt.
![Abbild 2: Scrum](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/5922575193603e9043a54e77b1ea2e48731e4f6e/docs/source/scrum.png)
Der Scrum-Prozess besteht aus:

1. **Product Backlog**: Liste aller gewünschten Arbeiten am Projekt, priorisiert durch den Product Owner.
2. **Sprint Planning Meeting**: Planungssitzung zu Beginn eines jeden Sprints, bei der das Team entscheidet, welche Aufgaben aus dem Product Backlog übernommen werden.
3. **Sprint Backlog**: Liste der Aufgaben, die im aktuellen Sprint erledigt werden sollen.
4. **Sprint**: Ein Zeitraum (in der Regel 1-4 Wochen), in dem ein nutzbares Produktinkrement erstellt wird.
5. **Daily Stand Up**: Tägliches kurzes Treffen, um den Fortschritt zu besprechen und mögliche Hindernisse zu identifizieren.
6. **Sprint Review**: Am Ende des Sprints präsentiert das Team das erstellte Produktinkrement und sammelt Feedback.
7. **Sprint Retrospective**: Sitzung zur Reflexion des Sprints, bei der das Team über Verbesserungsmöglichkeiten spricht.

Der iterative Zyklus von Scrum ermöglicht es dem Team, flexibel auf Veränderungen zu reagieren und kontinuierlich Verbesserungen vorzunehmen.

### Kanban Boards

Das folgende Abbild zeigt ein Kanban-Board, das zur Organisation der Aufgaben innerhalb eines Scrum-Sprints verwendet wird:

![Abbild 3: Kanban](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/Kanban.png?ref_type=heads)
In diesem Kanban-Board sind die verschiedenen Aufgaben nach ihrem aktuellen Status geordnet:

1. **Aufgaben**: Aufgaben, die noch nicht begonnen wurden.
2. **In Arbeit**: Aufgaben, an denen derzeit gearbeitet wird. 
3. **On Hold**: Aufgaben, die aus bestimmten Gründen vorübergehend pausiert sind. 
4. **Fertig**: Abgeschlossene Aufgaben. 

Jede Aufgabe ist mit einem spezifischen Tag versehen, der die Art der Aufgabe oder den zugehörigen Bereich kennzeichnet, wie beispielsweise "Dokumentation" oder "Backend". Die Initialen (z.B. JH) zeigen an, welcher Teammitglied die Aufgabe bearbeitet.

Dieses Kanban-Board veranschaulicht, wie Scrum-Teams ihre Aufgaben visuell organisieren und den Fortschritt in einem Sprint überwachen können. Es erleichtert die Transparenz und Zusammenarbeit innerhalb des Teams und stellt sicher, dass alle Mitglieder über den Status der verschiedenen Aufgaben informiert sind.

### Zeitplan

Das folgende Abbild zeigt eine detaillierte Ansicht der Aufgaben und Fortschritte in einem Scrum-Projektplan, organisiert in Sprints und Aufgabenstatus

![Abbild 4: Zeitplan](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/Zeitplan.png?ref_type=heads)
