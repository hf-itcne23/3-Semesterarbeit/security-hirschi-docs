# SEUSAG

Das folgende Abbild zeigt die Architektur und die Interaktionen der verschiedenen Komponenten in einer Anwendung, die auf einer EC2-Instanz bei AWS gehostet wird:
![Abbild 6: SEUSAG](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/seusag.png?ref_type=heads)

### Beschreibung der Komponenten und deren Interaktionen:

**Kunde:**
Der Kunde greift über einen Proxy auf die Anwendung zu (Schritt 1).
Der Kunde erhält die Informationen auf dem Frontend (Schritt 7).

**Proxy:**
Der Proxy leitet die Anfragen des Kunden an das Frontend der Anwendung weiter (Schritt 2).

**Frontend:**
Das Frontend verarbeitet die Anfragen und kommuniziert mit dem Backend, um die erforderlichen Daten zu erhalten (Schritt 3).
Das Frontend schickt die erhaltene Daten vom Backend weiter an den Kunden. (Schritt 7).

**Backend:**
Das Backend ruft die User Daten in der Datenbank ab oder erstellt diese. (Schritt 4)
Das Backend führt die Logik der Anwendung aus und kann bei Bedarf externe APIs aufrufen (Schritt 5).

**Datenbank:**
Speichert Login Daten des Kunden. (Schritt 4).

**Externe APIs:**
Das Backend kann verschiedene externe APIs aufrufen, um zusätzliche Daten oder Dienste zu nutzen, wie z.B. Virus Total, News, und Useless Fact. (Schritt 5).

**GitLab:**
GitLab wird für die kontinuierliche Integration und Bereitstellung verwendet.
Der Quellcode wird im GitLab-Repository gespeichert und von der Pipeline verarbeitet (Schritt 8).
Änderungen und Deployments werden von GitLab aus gesteuert und in die AWS-Umgebung (EC2-Instanz) integriert (Schritt 9).