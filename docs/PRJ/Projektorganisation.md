# Projektorganisation

![Abbild 1: Projektorganisation](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/projektorganisation.png?ref_type=heads)
### Rollenbeschreibung
#### Scrum Master

Der Scrum Master ist verantwortlich für die Implementierung und Einhaltung der Scrum-Methodik. Diese Rolle unterstützt das Team dabei, effizient zu arbeiten und Hindernisse zu beseitigen. Der Scrum Master moderiert Meetings, fördert die Selbstorganisation und coacht das Team in agilen Praktiken, um kontinuierliche Verbesserungen zu gewährleisten.

#### Product Owner

Der Product Owner trägt die Verantwortung für die Maximierung des Wertes des Produkts und die Verwaltung des Product Backlogs. Diese Rolle definiert und priorisiert Anforderungen, kommuniziert die Vision des Produkts und trifft Entscheidungen über Funktionen und Freigaben. Der Product Owner stellt sicher, dass das Team auf die wertvollsten Aufgaben fokussiert ist.

#### Fachexpert

Der Fachexpert verfügt über tiefgehende Kenntnisse und umfassende Erfahrung in einem speziellen Fachgebiet. Diese Rolle unterstützt Projekte durch die Bereitstellung von Fachwissen, Beratung und Problemlösungen. Der Fachexpert ist oft der Ansprechpartner für komplexe technische oder fachliche Fragen und trägt dazu bei, die Qualität und Genauigkeit von Projektergebnissen zu gewährleisten.

#### Business Owner

Der Business Owner ist die Person oder Abteilung, die für die strategische Ausrichtung und den geschäftlichen Erfolg eines Projekts oder Produkts verantwortlich ist. Diese Rolle umfasst die Definition von Geschäftszielen, die Sicherstellung der Übereinstimmung mit den Unternehmenszielen und die Überwachung des Mehrwerts für das Unternehmen.

#### Stakeholder

Stakeholder sind Einzelpersonen oder Gruppen, die ein Interesse am Ergebnis eines Projekts haben und davon betroffen sind. Dies können interne und externe Parteien sein, wie zum Beispiel Kunden, Mitarbeiter, Lieferanten oder Investoren. Stakeholder haben oft unterschiedliche Erwartungen und Anforderungen an das Projekt, und es ist wichtig, ihre Interessen zu identifizieren und zu managen, um den Projekterfolg sicherzustellen.
