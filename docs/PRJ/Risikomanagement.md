# Risikomanagement

Risikomanagement ist der Prozess der Identifizierung, Bewertung und Priorisierung von Risiken, gefolgt von koordinierten und wirtschaftlichen Anwendungen von Ressourcen, um die Wahrscheinlichkeit und Auswirkungen von unglücklichen Ereignissen zu minimieren oder zu kontrollieren. Es umfasst die folgenden Schritte:

1. **Risikoidentifikation:** Erkennen und Beschreiben von Risiken, die das Erreichen von Zielen beeinträchtigen könnten.
2. **Risikobewertung:** Analyse der Risiken hinsichtlich ihrer Eintrittswahrscheinlichkeit und potenziellen Auswirkungen.
3. **Risikobewältigung:** Entwickeln von Strategien zur Vermeidung, Reduzierung, Übertragung oder Akzeptanz der Risiken.
4. **Überwachung und Überprüfung:** Kontinuierliches Überwachen der Risiken und der Wirksamkeit der Maßnahmen sowie Anpassung der Strategien bei Bedarf.

Das Ziel des Risikomanagements ist es, Unsicherheiten zu verringern und eine stabile Grundlage für die Entscheidungsfindung zu schaffen.

#### 1. Risikoidentifikation

**Technische Risiken:**

- **API-Integrationsprobleme:** Schwierigkeiten bei der Integration von verschiedenen Security-APIs aufgrund von Inkompatibilitäten oder mangelnder Dokumentation.
- **Backend-Leistung:** Unzureichende Backend-Leistung, die zu langsamen Reaktionszeiten oder Systemausfällen führen könnte.
- **Sicherheitslücken im System:** Potenzielle Schwachstellen im Login-System oder in der gesamten Anwendung, die zu unbefugtem Zugriff führen könnten.

**Projektmanagement-Risiken:**

- **Zeitmanagement:** Verzögerungen im Projektplan aufgrund unerwarteter technischer Herausforderungen oder mangelnder Ressourcen.
- **Ressourcenengpässe:** Begrenzte Verfügbarkeit von qualifiziertem Personal für die Entwicklung und Implementierung.

**Usability-Risiken:**

- **Benutzerfreundlichkeit:** Schwierigkeiten bei der Gestaltung einer intuitiven und benutzerfreundlichen Oberfläche, die den Anforderungen der Benutzer gerecht wird.

#### 2. Risikobewertung


![Abbild 7: SEUSAG](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/risk.png?ref_type=heads)
*Leider gibt es Probleme bei Gitbook mit langen Tabellen, deswegen hier nur ein Bild. Am besten kurz Rechtsklick und "Bild in neuem Tab öffnen" auswählen.

#### 3. Risikobewältigung

**API-Integrationsprobleme:**

- **Strategie:** Durchführung einer gründlichen Recherche und Auswahl von APIs mit guter Dokumentation und Unterstützung. Prototyping und regelmäßige Tests der API-Integration.

**Backend-Leistung:**

- **Strategie:** Implementierung von Skalierbarkeit und Lasttests. Nutzung von Cloud-Diensten zur Unterstützung der Backend-Infrastruktur.

**Sicherheitslücken im System:**

- **Strategie:** Durchführung von regelmässigen Sicherheitsüberprüfungen und Penetrationstests. Implementierung bewährter Sicherheitspraktiken für die Entwicklung des Login-Systems.

**Zeitmanagement:**

- **Strategie:** Erstellung eines detaillierten Projektplans mit realistischen Meilensteinen. Pufferzeiten einplanen und regelmäßige Fortschrittskontrollen durchführen.

**Ressourcenengpässe:**

- **Strategie:** Schulung des vorhandenen Personals und gegebenenfalls Einstellung von externen Beratern oder Entwicklern. Priorisierung von Aufgaben und effiziente Ressourcenverteilung.

**Benutzerfreundlichkeit:**

- **Strategie:** Durchführung von Usability-Tests mit echten Benutzern. Einholen von Feedback und iterative Verbesserung der Benutzeroberfläche.

#### 4. Überwachung und Überprüfung

- **Regelmässige Fortschrittsbesprechungen:** Wöchentliche Meetings zur Überprüfung des Projektfortschritts und zur Identifizierung neuer Risiken.
- **Fortlaufende Tests:** Kontinuierliche Durchführung von Tests und Überprüfungen zur Sicherstellung der Systemleistung und -sicherheit.
- **Feedback-Schleifen:** Einholen von Benutzerfeedback und kontinuierliche Anpassung der Anwendung basierend auf den Rückmeldungen.


### Risikomatrix

|                        | Niedrig          | Mittel                                     | Hoch                        | Sehr Hoch |
| ---------------------- | ---------------- | ------------------------------------------ | --------------------------- | --------- |
| **Hoch**               | Zeitmanagement   | API-Integrationsprobleme, Backend-Leistung | Sicherheitslücken im System |           |
| **Mittel**             |                  | Benutzerfreundlichkeit, Ressourcenengpässe |                             |           |
| **Niedrig**            |                  |                                            |                             |           |
| **Sehr Niedrig**       |                  |                                            |                             |           |
| **Wahrscheinlichkeit** | **Sehr Niedrig** | **Niedrig**                                | **Mittel**                  | **Hoch**  |


Eine Risikomatrix hilft dabei, die identifizierten Risiken visuell darzustellen, um ihre Priorität basierend auf Wahrscheinlichkeit und Auswirkung zu bewerten.


