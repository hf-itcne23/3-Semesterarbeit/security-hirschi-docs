# Reflexion

Die Arbeit an meinem Projekt "Security auf einen Blick" war eine intensive und lehrreiche Erfahrung, die mir viele wertvolle Einblicke und Erkenntnisse gebracht hat. Von der ersten Idee bis zur finalen Umsetzung war der Prozess sowohl herausfordernd als auch unglaublich bereichernd.

Eine der grössten Herausforderungen bestand darin, eine benutzerfreundliche Oberfläche zu entwickeln, die den Bedürfnissen der Benutzer gerecht wird und gleichzeitig eine hohe Usability bietet. Die Wahl der Technologien und das Design der Benutzeroberfläche erforderten viel Überlegung und viele Anpassungen. Besonders das Troubleshooting im Frontend hat deutlich mehr Zeit in Anspruch genommen als erwartet. 

Im Backend war die Integration und Verarbeitung der Security-APIs eine komplexe Aufgabe. Die Auswahl und Implementierung geeigneter APIs wie VirusTotal erforderte gründliche Recherchen und viele Tests. Hier habe ich gelernt, wie wichtig es ist, die Dokumentation der APIs genau zu verstehen und die spezifischen Anforderungen der Sicherheitsanalyse zu berücksichtigen. Diese Arbeit hat meine Fähigkeiten im Bereich der API-Integration und Backend-Entwicklung erheblich erweitert.

Die Implementierung des Login-Systems mit Authentifizierung und Autorisierungsfunktionen war ebenfalls eine wichtige und herausfordernde Aufgabe. Es war entscheidend, die Sicherheit der Benutzerkonten zu gewährleisten und unbefugten Zugriff zu verhindern. 

Während des gesamten Projekts wurde mir auch die Bedeutung einer soliden Projektplanung und -organisation bewusst. Das Arbeiten mit verschiedenen Technologien und die Integration dieser in einer zusammenhängenden Anwendung erforderten sorgfältige Planung und gutes Zeitmanagement. Die Nutzung von GitLab für die Versionskontrolle und CI/CD-Pipelines war äusserst hilfreich, um den Entwicklungsprozess effizient zu gestalten.

Rückblickend war die Arbeit an diesem Projekt eine wertvolle Lerngelegenheit. Ich habe nicht nur meine technischen Fähigkeiten verbessert, sondern auch meine Fähigkeiten im Projektmanagement und in der Problemlösung weiterentwickelt. Die vielen Stunden, die ich in dieses Projekt investiert habe, haben sich gelohnt, und die gewonnenen Erkenntnisse und Erfahrungen werden in zukünftigen Projekten von grossem Nutzen sein.

Insgesamt bin ich stolz auf die Fortschritte, die ich gemacht habe, und die Ergebnisse, die ich erzielt habe. Diese Arbeit hat mir gezeigt, wie wichtig es ist, sich kontinuierlich weiterzubilden und offen für neue Technologien und Methoden zu sein. Die Erkenntnisse aus diesem Projekt werden mir helfen, in meiner beruflichen Laufbahn erfolgreich zu sein und weiterhin zur Verbesserung der Informationssicherheit beizutragen.