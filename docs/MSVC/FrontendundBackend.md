# Frontend und Backend

Das Herzstück meiner App bildet sowohl das Frontend als auch das Backend. Beide Komponenten sind essenziell, um eine reibungslose und effiziente Funktionalität der Anwendung zu gewährleisten.

### Funktionen:

1. **Anmeldefunktion**:
    - Ermöglicht es nur autorisierten Benutzern, sich anzumelden und Abfragen durchzuführen.
    - Gewährleistet die Sicherheit und Integrität der Daten durch Benutzerautorisierung und -authentifizierung.
2. **Bearbeitung von Anfragen und Zugriff auf externe APIs**:
    - Die App kann Anfragen von Benutzern verarbeiten und entsprechende Antworten liefern.
    - Sie integriert externe APIs, um zusätzliche Daten oder Funktionen zu nutzen, die nicht nativ in der App vorhanden sind.

### CI/CD-Pipeline:

Die kontinuierliche Integration und Bereitstellung (CI/CD) ist ein zentraler Bestandteil des Entwicklungsprozesses. Jede Änderung am Code löst automatisch die Pipeline aus, die folgende Schritte umfasst:

1. **Testing**:
    - **Benutzerverwaltung**:
        - Überprüft, ob neue Benutzer korrekt erfasst und bestehende Benutzer sich einloggen können.
    - **Externe APIs**:
        - Testet die Verbindungen zu externen APIs, um sicherzustellen, dass sie ordnungsgemäß funktionieren und die erwarteten Daten liefern.
2. **Build**:
    - Der Code aus dem GitLab-Repository wird verwendet, um ein Docker-Image zu erstellen.
    - Dieses Docker-Image enthält alle notwendigen Komponenten und Abhängigkeiten der App, um sie in einer isolierten und konsistenten Umgebung auszuführen.
3. **Deploy**:
    - Die erstellte Konfiguration und das Docker-Image werden auf die virtuelle Maschine (VM) übertragen.
    - Dies stellt sicher, dass die neueste Version der App reibungslos und zuverlässig in der Produktionsumgebung läuft.

Durch diese strukturierte und automatisierte Pipeline wird sichergestellt, dass jede Änderung am Code gründlich getestet, gebaut und bereitgestellt wird. Dies minimiert das Risiko von Fehlern und Ausfallzeiten und verbessert die Gesamtqualität und Zuverlässigkeit der App.

### How to Deploy:

#### Lokale Ausführung

Es ist möglich, den Code lokal auszuführen. Hierzu sind folgende Schritte erforderlich:

1. **Repository kopieren**:
    - Klonen Sie das Repository auf Ihre lokale Maschine.
2. **Docker Compose ausführen**:
    - Für die Produktionsumgebung:
        `docker compose compose.prod.yaml up --build`
    - Für die Entwicklungsumgebung:
        `docker compose compose.yaml up`
        

#### Ausführung auf einer Linux VM

Falls Sie die Anwendung auf einer Linux-VM ausführen möchten, müssen einige Umgebungsvariablen angepasst werden:

1. **SSH-Schlüssel konfigurieren**:
    - SSH_Private_KEY (Schlüssel von der Erstellung der VM)
    - SSH_Host_Key
        `sudo ssh-keygen -l -f ~/.ssh/authorized_keys`
2. **Weitere notwendige Variablen**:
    - DEPLOY_TARGET (IP-Adresse der VM)
    - CI_REGISTRY (registry.gitlab.com)
    - CI_REGISTRY_PASSWORD (Benutzertoken)
    - CI_REGISTRY_USER (GitLab-Benutzername)

Durch die Anpassung dieser Variablen können Sie sicherstellen, dass die Anwendung korrekt auf der VM bereitgestellt und ausgeführt wird.


#### Source Code
Der Source Code findet man hier:

[GitLab](https://gitlab.com/hf-itcne23/3-Semesterarbeit/3-semesterarbeit-jan-hirschi) 