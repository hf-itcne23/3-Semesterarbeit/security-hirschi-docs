# Externe API

In meiner App wurden verschiedene externe APIs integriert, um zusätzliche Funktionalitäten bereitzustellen. Hier sind die APIs, die verwendet werden, einschließlich Links zur Dokumentation:
### VirusTotal: Für URL-Scans

Dokumentation: [VirusTotal API Documentation](https://docs.virustotal.com/reference/overview)
Diese API wird verwendet, um URLs auf Malware und schädliche Inhalte zu scannen.
#### Numverify: Für die Telefonnummernvalidierung

Dokumentation: [Numverify API Documentation](https://numverify.com/documentation)
Diese API überprüft, ob Telefonnummern gültig sind und liefert zusätzliche Informationen wie den Telefonanbieter und den Standort.
#### NewsAPI: Für Nachrichten im Bereich Cybersecurity

Dokumentation: [NewsAPI Documentation](https://newsapi.ai/documentation?tab=introduction)
Diese API bietet aktuelle Nachrichtenartikel und Berichte zu Cybersecurity-Themen.
#### UselessFacts: Für lustige und unnütze Fakten

Dokumentation: [UselessFacts API Documentation](https://uselessfacts.jsph.pl/)
Diese API liefert unterhaltsame, unnütze Fakten, um die Benutzer zu amüsieren.