# Übersicht der App

Im Folgenden wird die Funktionalität der App detailliert beschrieben:

### Zugang zur App

Wenn der Nutzer die Website [security-hirschi.ch](https://security-hirschi.ch) besucht, wird er folgende Startseite sehen:

![Startseite](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/home.png?ref_type=heads)

### Registrierung

Um die Funktionen der App nutzen zu können, muss sich der Nutzer zunächst registrieren:

![Registrierung](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/register.png?ref_type=heads)

**Registrierungsfelder:**
- **Benutzername:** Einzigartiger Username
- **E-Mail:** Einzigartige E-Mail-Adresse
- **Passwort:** Passwort
- **Einladungstoken:** Einladungstoken erforderlich

Der Registrierungsvorgang stellt sicher, dass nur autorisierte Benutzer Zugriff auf die App haben. Jeder Benutzername und jede E-Mail-Adresse muss einzigartig sein, um Verwechslungen zu vermeiden und die Sicherheit zu gewährleisten. Der Einladungstoken dient dazu, den Zugang zu beschränken und sicherzustellen, dass nur eingeladene Benutzer sich registrieren können.

### Anmeldung

Falls der Nutzer bereits ein Konto besitzt, kann er sich über das Login-Formular anmelden:

![Login](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/login.png?ref_type=heads)

**Anmeldefelder:**
- **Benutzername:** Username
- **Passwort:** Passwort

Der Anmeldevorgang ist einfach und sicher. Der Benutzer muss seinen zuvor registrierten Benutzernamen und sein Passwort eingeben. Dies stellt sicher, dass nur autorisierte Benutzer auf die App zugreifen können.

### Hauptfunktionen der App

Nach der Anmeldung gelangt der Nutzer über den Link "Security News / Tools" auf die Hauptseite der App, die folgende Funktionen bietet:

![Startseite new](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/home2.png?ref_type=heads)

#### Cybersecurity News

Auf der linken Seite werden die neuesten Cybersecurity-Nachrichten angezeigt:

![Cybersecurity News](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/security.png?ref_type=heads)

Diese Sektion hält den Nutzer über aktuelle Entwicklungen und Bedrohungen im Bereich der Cybersecurity auf dem Laufenden. Die Nachrichten sind stets aktuell und bieten wertvolle Informationen für alle, die sich für Cybersicherheit interessieren.

#### URL-Prüfung

Auf der rechten Seite kann der Nutzer eine URL eingeben, um sie auf Sicherheit zu überprüfen:

![URL-Prüfung](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/url.png?ref_type=heads)

Der URL-Prüfdienst analysiert die eingegebene URL und liefert eines der folgenden Ergebnisse:

1. **No malicious or suspicious content found:** Die URL ist sicher und enthält keine schädlichen Inhalte.
2. **Suspicious content found:** Die URL weist verdächtige Merkmale auf, die auf mögliche Sicherheitsrisiken hindeuten.
3. **Malicious content found:** Die URL ist bösartig und stellt eine Bedrohung dar.

Diese Funktion ist besonders nützlich, um sicherzustellen, dass Nutzer vor dem Besuch von Webseiten gewarnt werden, die potenziell gefährlich sein könnten.

#### Mobile Nummer Prüfung

Eine weitere Funktion ist der Mobile Nummer Prüfer:

![Mobile Nummer Prüfung](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/phone.png?ref_type=heads)

Hier kann der Nutzer überprüfen, ob eine Telefonnummer echt ist, aus welchem Land sie stammt und welcher Anbieter dahintersteht. Diese Funktion hilft dabei, Betrugsversuche zu erkennen und die Echtheit von Telefonnummern zu verifizieren.

#### Useless Fact Button

Zum Schluss gibt es noch den "Useless Fact" Button, mit dem der Nutzer unnützes Wissen abrufen kann:

![Useless Fact Button](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/raw/master/docs/source/facts.png?ref_type=heads)

Dieser Button liefert dem Nutzer zufällige, oft amüsante und unnütze Fakten. Es ist eine unterhaltsame Funktion, die dem Nutzer interessante Trivia bietet, die möglicherweise keinen praktischen Nutzen haben, aber dennoch Spaß machen können.
