# Technologien

Für die Umsetzung des Projekts habe ich zunächst verschiedene Technologien verglichen, um die bestmöglichen Werkzeuge für meine Anforderungen zu finden. Nach sorgfältiger Evaluierung habe ich mich für folgende Technologien entschieden:

### Frontend

#### APIFlask Template

**Hauptseite:** [APIFlask](https://apiflask.com/)

- **Vorteile:**
    - Einfach zu nutzen für kleinere Anwendungen und schnelle Prototypenerstellung.
    - Integrierte Unterstützung für API-Dokumentation.
- **Nachteile:**
    - Weniger Flexibilität und Features im Vergleich zu umfangreicheren Frameworks wie React oder Vue.js.
    - Geringere Community und weniger Ressourcen.
- **Warum gewählt:** APIFlask Template bietet eine einfache und schnelle Möglichkeit, APIs zu entwickeln und zu dokumentieren, was für den Fokus auf Security-APIs sehr vorteilhaft ist.

#### Alternative: Vue.js

**Hauptseite:** [Vue.js](https://vuejs.org/)

- **Vorteile:**
    - Leichtgewichtig und einfach zu erlernen.
    - Flexibel und modular, gut für die Erstellung interaktiver Benutzeroberflächen.
    - Große Community und viele Ressourcen.
- **Nachteile:**
    - Für kleine Projekte kann es überdimensioniert sein.
    - Zusätzlicher Aufwand für die Integration und Einrichtung im Vergleich zu APIFlask Template.
- **Warum nicht gewählt:** Für den spezifischen Fokus auf eine schnelle und einfache API-Dokumentation und -Nutzung bietet APIFlask Template eine direktere und weniger komplexe Lösung.

### Backend

#### API Flask

**Hauptseite:** Flask

- **Vorteile:**
    - Minimalistisch und leichtgewichtig.
    - Flexibel und einfach zu erweitern.
    - Große Community und viele Erweiterungen.
- **Nachteile:**
    - Manuelle Skalierung und Verwaltung von größeren Anwendungen kann komplex sein.
    - Fehlende integrierte Tools für größere Projekte im Vergleich zu Frameworks wie Django.
- **Warum gewählt:** Flask bietet die notwendige Flexibilität und Einfachheit für schnelle Entwicklung und Anpassung, was ideal für die Integration und Verarbeitung von Security-APIs ist.

### Datenbank

#### MySQL

**Hauptseite:** [MySQL](https://www.mysql.com/)

- **Vorteile:**
    - Weit verbreitet und gut unterstützt.
    - Leistungsstark für komplexe Abfragen und große Datenmengen.
    - Gute Dokumentation und viele Ressourcen.
- **Nachteile:**
    - Höherer Verwaltungsaufwand im Vergleich zu SQLite.
    - Kann bei extrem großen Datenmengen langsamer werden als einige NoSQL-Datenbanken.
- **Warum gewählt:** MySQL bietet eine robuste und skalierbare Lösung für die Datenverwaltung, was für die Speicherung und Verarbeitung von API-Daten und Benutzerinformationen wichtig ist.

#### Alternative: PostgreSQL

**Hauptseite:** [PostgreSQL](https://www.postgresql.org/)

- **Vorteile:**
    - Hoch skalierbar und erweiterbar.
    - Unterstützt erweiterte SQL-Funktionen und komplexe Abfragen.
    - Open Source und weit verbreitet.
- **Nachteile:**
    - Etwas komplizierter einzurichten und zu warten als MySQL.
    - Kann bei bestimmten Arbeitslasten langsamer sein.
- **Warum nicht gewählt:** Für die Anforderungen dieses Projekts bietet MySQL eine ausreichende Funktionalität und ist einfacher in der Verwaltung, was für den schnellen Entwicklungszyklus von Vorteil ist.

### Deployment

#### Docker

**Hauptseite:** [Docker](https://www.docker.com/)

- **Vorteile:**
    - Einfaches Management von Entwicklungs- und Produktionsumgebungen.
    - Skalierbarkeit und Portabilität von Anwendungen.
    - Große Community und umfangreiche Dokumentation.
- **Nachteile:**
    - Lernkurve für komplexere Anwendungsfälle.
    - Erfordert zusätzliche Ressourcen für das Container-Management.
- **Warum gewählt:** Docker ermöglicht eine konsistente Umgebung über verschiedene Entwicklungs- und Produktionssysteme hinweg und unterstützt die einfache Skalierung und Verwaltung der Anwendung.

#### Alternative: Podman

**Hauptseite:** [Podman](https://podman.io/)

- **Vorteile:**
    - Rootless Container Management, sicherer als Docker.
    - Kompatibel mit Docker-CLI.
    - Keine Notwendigkeit für einen Daemon, was sicherer ist.
- **Nachteile:**
    - Kleinere Community und weniger Dokumentation als Docker.
    - Einige Docker-Funktionen fehlen oder sind anders implementiert.
- **Warum nicht gewählt:** Docker bietet eine breitere Unterstützung und eine größere Community, was die Fehlersuche und den Support erleichtert.

### Versionskontrolle

#### GitLab

**Hauptseite:** [GitLab](https://about.gitlab.com/)

- **Vorteile:**
    - Integrierte CI/CD-Pipelines.
    - Umfangreiche Features für DevOps und Projektmanagement.
    - Open Source und Self-Hosting-Option.
- **Nachteile:**
    - Komplexer im Vergleich zu GitHub für neue Benutzer.
    - Kann ressourcenintensiv sein, wenn selbst gehostet.
- **Warum gewählt:** GitLab bietet umfassende DevOps-Funktionen und integrierte CI/CD-Pipelines, die die Entwicklung und Bereitstellung der Anwendung effizienter gestalten.

#### Alternative: GitHub

**Hauptseite:** [GitHub](https://github.com/)

- **Vorteile:**
    - Große Entwickler-Community.
    - Umfangreiche Integrationen und Marketplace für Erweiterungen.
    - GitHub Actions für CI/CD-Workflows.
- **Nachteile:**
    - Weniger integrierte DevOps-Tools im Vergleich zu GitLab.
    - Private Repositories sind nur in kostenpflichtigen Plänen unbegrenzt.
- **Warum nicht gewählt:** GitLab bietet integrierte DevOps-Funktionen und CI/CD-Pipelines, die die Entwicklung und Bereitstellung der Anwendung effizienter gestalten.

### Cloud

#### AWS (Amazon Web Services) Learner Lab

**Hauptseite:** [AWS](https://aws.amazon.com/)

- **Vorteile:**
    - Umfassende Palette von Cloud-Diensten.
    - Hohe Skalierbarkeit und Verfügbarkeit.
    - Umfangreiche Dokumentation und Community-Support.
- **Nachteile:**
    - Komplexe Preismodelle.
    - Kann für kleine Projekte überdimensioniert und kostspielig sein.
- **Warum gewählt:** AWS bietet eine Vielzahl von Diensten, die eine flexible und skalierbare Cloud-Infrastruktur ermöglichen, was ideal für die Anforderungen eines Sicherheitsprojekts ist.

#### Alternative: Azure (Microsoft Azure)

**Hauptseite:** [Azure](https://azure.microsoft.com/)

- **Vorteile:**
    - Umfassende Palette von Cloud-Diensten.
    - Gute Integration mit Microsoft-Produkten.
    - Starke Angebote für Big Data und Machine Learning.
- **Nachteile:**
    - Komplexe Preismodelle.
    - Kann für kleinere Projekte überdimensioniert sein.
- **Warum nicht gewählt:** AWS bietet eine breitere Palette von Diensten und eine höhere Verfügbarkeit von Dokumentation und Community-Support, was für die Flexibilität und Skalierbarkeit des Projekts von Vorteil ist.

Durch die sorgfältige Evaluierung und Auswahl dieser Technologien konnte ich sicherstellen, dass mein Projekt technisch solide und effizient umgesetzt wird, während gleichzeitig meine spezifischen Bedürfnisse erfüllt werden.