# Anforderungen

Mit meiner Semesterarbeit möchte ich eine übersichtliche Darstellung verschiedener Security-Tools bieten. Obwohl es bereits viele Seiten gibt, die eine Vielzahl von Informationen anbieten, möchte ich eine eigene Seite erstellen, die speziell auf meine Bedürfnisse zugeschnitten ist.

### Funktionale Anforderungen

#### Frontend:

**Benutzerfreundliche Oberfläche:**
Intuitive Navigation: Klare Menüführung und übersichtliche Layouts.

#### Backend:

**Effiziente Architektur:**
Modulare Struktur: Trennung der Geschäftslogik, Datenverarbeitung und API-Integration.

**Integration von Security-APIs:**
Unterstützung mehrerer APIs: Möglichkeit zur einfachen Hinzufügung neuer APIs.

#### Login-System:

**Benutzerkonten und Authentifizierung:**
Registrierung und Anmeldung: Sichere Registrierung und Anmeldeprozesse für Benutzer.

**Autorisierung:**
Zugriffskontrollen: Sicherstellung, dass nur autorisierte Benutzer bestimmte Aktionen durchführen können.

**Sicherheit:**
Verschlüsselung: Verschlüsselung sensibler Daten (Password Hashing)

### Nicht-funktionale Anforderungen

**Leistung:**
Schnelle Ladezeiten: Optimierung der Anfragen und Minimierung der Antwortzeiten.
Verfügbarkeit: Hohe Verfügbarkeit des Systems, mit minimalen Ausfallzeiten.

**Sicherheit:**
 Datensicherheit: Schutz der Benutzerdaten und der API-Schlüssel.
 Regelmässige Sicherheitsupdates: Anwendung von Patches und Updates, um Sicherheitslücken zu schließen.
 
**Wartbarkeit:**
Dokumentation: Ausführliche Dokumentation des Codes und der Systemarchitektur.
 Testbarkeit: Implementierung von Unit- und Integrationstests zur Sicherstellung der Systemstabilität.

### Technische Anforderungen

**Technologie-Stack:**
 Frontend: Verwendung von modernen Webtechnologien wie HTML5, CSS3, JavaScript.
 Backend: Einsatz von serverseitigen Technologien wie Python (APIFlask)
Datenbank: Nutzung von relationalen Datenbanken (MySQL).

**APIs:**
API-Integration: Anbindung und Nutzung von Security-APIs wie VirusTotal etc.

**Deployment:**
Cloud-Plattform: Einsatz von Cloud-Diensten wie AWS, Azure oder Google Cloud für Hosting und Skalierung.
CI/CD-Pipeline: Implementierung einer Continuous Integration und Continuous Deployment Pipeline zur Automatisierung der Build- und Deployment-Prozesse.