# Security und Datenschutz

In der heutigen digitalen Ära sind Security und Datenschutz zentrale Aspekte jeder Webanwendung. Um die Sicherheit der Benutzerdaten und die Einhaltung von Datenschutzrichtlinien zu gewährleisten, werden spezifische Massnahmen ergriffen.

### Sicherheitsmassnahmen: Login-System und Passwort-Hashing

Um unbefugten Zugriff zu verhindern, wird ein robustes Login-System implementiert. Dabei kommen folgende Sicherheitsmaßnahmen zum Einsatz:

- **Passwort-Hashing**: Anstatt die Passwörter der Benutzer im Klartext zu speichern, werden sie mittels sicherer Hash-Algorithmen (z.B. werkzeug.security) verschlüsselt. Dadurch wird sichergestellt, dass selbst im Falle eines Datenlecks die Passwörter nicht im Klartext zugänglich sind.

### Datenschutz: Speicherung und Nutzung von Benutzerdaten

Die Webanwendung speichert nur die notwendigsten Benutzerdaten, um den Datenschutz zu gewährleisten und Missbrauch zu minimieren. Dies umfasst:

- **Benutzername und E-Mail-Adresse**: Diese Daten werden zusammen mit dem Passwort-Hash gespeichert und ausschliesslich für das Login-System verwendet. Der Benutzername dient der Identifikation, während die E-Mail-Adresse für Kommunikationszwecke genutzt werden kann, 

- **Passwort-Hash**: Anstelle des Klartextpassworts wird nur der verschlüsselte Hash des Passworts gespeichert, um die Sicherheit der Benutzerdaten zu gewährleisten.

### Datenlöschung auf Anfrage

Im Einklang mit Datenschutzrichtlinien und -gesetzen (wie der DSGVO) wird den Benutzern die Möglichkeit geboten, ihre Daten auf Anfrage zu löschen. Dies umfasst:

- **Benutzerdaten löschen**: Auf Anforderung können alle gespeicherten Daten eines Benutzers, einschliesslich Benutzername, E-Mail-Adresse und Passwort-Hash, vollständig aus der Datenbank entfernt werden.

Diese Massnahmen stellen sicher, dass die Sicherheit und der Datenschutz der Benutzerdaten höchste Priorität haben, und bieten den Benutzern die Kontrolle über ihre eigenen Informationen.