# Servicekosten

### AWS Learner Lab:

- **Elastic IP:**
    - **Kosten**: Elastic IPs sind kostenlos, solange sie an eine laufende EC2-Instanz gebunden sind. Es fallen jedoch Gebühren von $0.005 pro Stunde an, wenn die Elastic IP nicht an eine laufende Instanz gebunden ist.
- **Linux VM:**
    - **Kosten**: Das AWS Free Tier ermöglicht es neuen AWS-Kunden, eine t2.micro-Instanz in der EC2-Serviceklasse kostenlos zu nutzen. Dies umfasst sowohl Linux- als auch Windows-Instanzen.
- **Docker:**
    - **Kostenlos**: Docker selbst verursacht keine direkten Kosten.
- **MySQL-Datenbank:**
    - **Kostenlos**: MySQL ist eine Open-Source-Datenbank und verursacht keine Lizenzgebühren.
- **SSL-Zertifikat:**
    - **Kostenlos**: SSL-Zertifikate von Let's Encrypt sind kostenlos.

### Domain

- **Hostpoint Domain (security-hirschi.ch)**: CHF 5.- für ein Jahr.

### Externe API

- **VirusTotal**: 
	- **Kostenlos**: 4 Lookups / min, 500 Lookups / day, 15.5k Lookups / month
- **Numverify:**
	- **Kostenlos:** 100 Lookups / month
- **NewsAPI:**
	- **Kostenlos:** 2000 Lookups / Month
- **Uselessfacts:**
	- **Kostenlos:** unbegrenzt

### Gesamtkosten:

Da ich das AWS Leaner Lab verwende, entstehen keine AWS Kosten. Die einzigen Kosten die ich hatte war die Domain mit **CHF 5.- pro Jahr.