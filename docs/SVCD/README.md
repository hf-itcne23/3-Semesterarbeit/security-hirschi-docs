# Service Design

* [Anforderungen](Anforderungen.md)
* [Technologien](docs/SVCD/Technologien.md)
* [Architektur](docs/SVCD/Architektur.md)
* [Security und Datenschutz](SecurityundDatenschutz.md)
* [Servicekosten](docs/SVCD/Servicekosten.md)