
# Security auf einen Blick
---
In der heutigen digitalen Landschaft stehen Unternehmen vor der Herausforderung, ihre Informationssicherheit zu gewährleisten. Obwohl es viele Security-Seiten und -Tools gibt, fehlt oft eine zentrale, umfassende Sammlung von Sicherheitsressourcen. Dies erschwert die effektive Auswahl und Nutzung von Sicherheitsquellen und erhöht das Risiko von Sicherheitslücken.

### Ausgangslage:

Die Semesterarbeit konzentriert sich darauf, eine Sammlung nützlicher Security-APIs zusammenzustellen. Der Fokus liegt darauf, die Funktionalität dieser APIs zu demonstrieren und ihre praktische Anwendung zu erforschen.

### Potenzial der Arbeit:

Die Arbeit bietet die Möglichkeit, verschiedene Security-APIs zu erkunden. Durch die Untersuchung der VirusTotal-API und ähnlicher Tools können wichtige Einblicke in die Möglichkeiten und Grenzen dieser Technologien gewonnen werden, was für Unternehmen und Organisationen von grossem Wert sein kann, um ihre Informationssicherheit zu stärken.

### Ziele:

**Frontend:**
Entwicklung einer benutzerfreundlichen Oberfläche, um die Bedürfnisse der Benutzer zu erfüllen und eine hohe Usability zu gewährleisten.

**Backend:**
Implementierung einer effizienten Backend-Architektur, die die Integration und Verarbeitung von Security-APIs ermöglicht.

**Login-System:**
Umsetzung eines Login-Systems mit Authentifizierung und Autorisierungsfunktionen, um die Sicherheit der Benutzerkonten zu gewährleisten und unbefugten Zugriff zu verhindern.

### Dokumente: 
[Einreichungsformular](https://gitlab.com/hf-itcne23/3-Semesterarbeit/security-hirschi-docs/-/blob/master/docs/source/Einreichungsformular.pdf)
### Projektmanagement:
[Jira](https://jan-hirschi-semesterarbeit.atlassian.net/jira/software/projects/SCRUM/boards/1/timeline)