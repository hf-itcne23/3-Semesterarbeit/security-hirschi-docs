# Summary

* [Einleitung](README.md)
* [Disclaimer](Disclaimer.md)
* [Projektmanagement](docs/PRJ/README.md)
  * [Projektorganisation](docs/PRJ/Projektorganisation.md)
  * [Projektmethode](docs/PRJ/Projektmethode.md)
  * [Sprints](docs/PRJ/Sprint.md)
  * [Seusag](docs/PRJ/SEUSAG.md)
  * [SWOT Analyse](docs/PRJ/SWOT.md)
  * [Risikomanagement](docs/PRJ/Risikomanagement.md)
* [Service Design](docs/SVCD/README.md)
  * [Anforderungen](docs/SVCD/Anforderungen.md)
  * [Technologien](docs/SVCD/Technologien.md)
  * [Architektur](docs/SVCD/Architektur.md)
  * [Security und Datenschutz](docs/SVCD/SecurityundDatenschutz.md)
  * [Servicekosten](docs/SVCD/Servicekosten.md)
* [Microservice](docs/MSVC/README.md)
  * [Frontend und Backend](docs/MSVC/FrontendundBackend.md)
    * [Uebersicht der App](docs/MSVC/Uebersicht.md)
  * [Externe API](docs/MSVC/ExterneAPI.md)
* [Schlussteil](docs/END/README.md)
  * [Fazit](docs/END/Fazit.md)
  * [Reflexion](docs/END/Reflexion.md)




