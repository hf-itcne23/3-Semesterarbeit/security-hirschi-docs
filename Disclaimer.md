# Disclaimer

Diese Dokumentation der Semesterarbeit wurde mithilfe von ChatGPT, einem Sprachmodell entwickelt von OpenAI, erstellt und verbessert. ChatGPT dient als ein Werkzeug zur Unterstützung bei der Generierung von Texten und bietet sprachliche Vorschläge basierend auf den bereitgestellten Informationen.

Es ist wichtig anzumerken, dass ChatGPT als unterstützendes Werkzeug verwendet wurde und nicht als alleinige Quelle für die Erstellung dieser Arbeit. Die Verantwortung für die Inhalte und die Qualität der Arbeit liegt beim Autor bzw. der Autorin, während ChatGPT lediglich als Hilfsmittel zur Verbesserung und Inspiration diente.

Sämtliche Codes-Ausschnitte in dieser Arbeit stammen aus dem Code des Projekts https://gitlab.com/hf-itcne23/3-Semesterarbeit/3-semesterarbeit-jan-hirschi.